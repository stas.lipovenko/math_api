from flask import Flask
from flask_restplus import Api
import flask_monitoringdashboard as dashboard

from core.klarna_math.app import km

api = Api(
    version='0.1',
    title='Klarna math WS',
    contact_email='stas.lipovenko@gmail.com'
)

api.add_namespace(km)


def create_app():
    app = Flask(__name__)
    api.init_app(app)
    dashboard.bind(app)
    return app
