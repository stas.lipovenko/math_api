from flask import make_response
from flask_restplus import Namespace, Resource
import json

# supportive libraries
from timeit import default_timer as timer
from functools import lru_cache
from math import sqrt, factorial
from functools import lru_cache
from typing import Mapping

km = Namespace(
    'klarna_math',
    description="""
    Web Services designed to calculate Fibonacci number,
    The Ackermann function and Factorial""",
)


def validate_int_param(n: str) -> int:
    """Makes int number from string parameter"""
    try:
        n = int(n)
        if n < 0:
            raise ValueError
        return n
    except ValueError:
        raise ValueError('Parameter must be a non-negative integer')


def make_json_result(result, time) -> str:
    """Make result is json format. Including time elapsed

    Arguments:
        result
        time

    Returns:
        json formatted string
    """
    return json.dumps({'result': result, 'time_elapsed': time})


@lru_cache()
def fibonnaci(n: int) -> int:
    """ Calculates Fibonnaci number placed on n position

    Arguments:
        n {int} -- Fibonacci number position

    Returns:
        int -- Fibonacci number at position n
    """
    # Let's use formula. Not recursive solution though,
    # but delivers it lightning fast
    fib_number = (((1 + sqrt(5)) ** n) - ((1 - sqrt(5)) ** n)) / (2 ** n * sqrt(5))
    return round(fib_number)


# just to reduce cpu and ram consumption. Good practice for recursive functions
@lru_cache(None)
def ackermann(m: int, n: int) -> int:
    """ Calculates Ackermann's function value

    Arguments:
        m {int}
        n {int}

    Returns:
        int -- Ackermann's function value

    !!!use sys.setrecursionlimit() to make it work with bigger m and n!!!
    """
    if m == 0:
        return n + 1
    elif n == 0:
        return ackermann(m - 1, 1)
    else:
        return ackermann(m - 1, ackermann(m, n - 1))


@km.route('/fibonacci/<n>',
          endpoint='fibonacci',
          doc={'params': {
              'n': 'Fibonacci number position'
          }})
class Fibonacci(Resource):
    def get(self, n):
        """ Method to calculate Fibonacci number by its position number """
        start = timer()
        try:
            # make validations prior to calling the calculation
            n = validate_int_param(n)
            response = make_response(
                make_json_result(fibonnaci(n),
                                 timer() - start), 200)
            response.headers = {'Content-type': 'application/json'}
        except Exception as e:
            response = make_response(json.dumps({'error': str(e)}), 500)
        return response


@km.route('/ackermann/<m>/<n>',
          endpoint='ackermann',
          doc={'params': {
              'm': 'M argument',
              'n': 'N argument'
          }})
class Ackermann(Resource):
    def get(self, m, n):
        """ Method to calculate Ackermann function """
        start = timer()
        try:
            # make validations prior to calling the calculation
            m = validate_int_param(m)
            n = validate_int_param(n)
            response = make_response(
                make_json_result(ackermann(m, n),
                                 timer() - start), 200)
            response.headers = {'Content-type': 'application/json'}
        except Exception as e:
            response = make_response(json.dumps({'error': str(e)}), 500)

        return response


@km.route('/factorial/<n>',
          endpoint='factorial',
          doc={'params': {
              'n': 'N argument'
          }})
class Factorial(Resource):
    def get(self, n):
        """ Method to calculate Factorial """
        start = timer()
        try:
            # make validations prior to calling the calculation
            n = validate_int_param(n)
            response = make_response(
                make_json_result(factorial(n),
                                 timer() - start), 200)
            response.headers = {'Content-type': 'application/json'}
        except Exception as e:
            response = make_response(json.dumps({'error': str(e)}), 500)

        return response
