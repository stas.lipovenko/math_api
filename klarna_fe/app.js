let app = new Vue({
    data: {
        fib_n: 0,
        ack_n: null,
        ack_m: null,
        fact_m: 0,
        fib_result: 0,
        ack_result: 0,
        fact_n: 0,
        fact_result: 0
    },
    methods: {
        calculateFibonacci: function() {
            axios.get(`http://127.0.0.1:8000/klarna_math/fibonacci/${this.fib_n}`).
                catch(
                    () => {
                        this.fib_result = 'Wrong input'
                    }
                ).
                then(
                    (response) => {
                        this.fib_result = response.data['result']
                    }
                )
        },
        calculateAckermann: function() {
            axios.get(`http://127.0.0.1:8000/klarna_math/ackermann/${this.ack_m}/${this.ack_n}`).
                catch(
                    () => {
                        this.ack_result = 'Wrong input'
                    }
                ).
                then(
                    response => {
                        this.ack_result = response.data['result']
                    }
                )
        },
        calculateFactorial: function() {
            axios.get(`http://127.0.0.1:8000/klarna_math/factorial/${this.ack_n}`).
                catch(
                    () => {
                        this.ack_result = 'Wrong input'
                    }
                ).
                then(
                    response => {
                        this.fact_result = response.data['result']
                    }
                )
        }
    }

});

// app.$mount('#app')

function mount_app_to_div(event) {
    app.$mount('#app');
}