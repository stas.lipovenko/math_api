## Solution description
I decided to use Flask framework as it's quite lightweight and powerful.
It's simple to start, supports WSGI and has a lot of features in building routes, validations and has built in testing engine
Besides that there is special flask restplus module that allows auto creating API docs based on Swagger/OAS notation

Yes, all this looks quite overhead, but provides great environment for extending functionality and easy to maintain and support.


## Klarna math project description and installation instructions

Project is developed under Ubuntu 19.10 using Python 3.7
Project is dsitributed via zip archive klarna_math.zip containing

├── core
│   ├── __init__.py
│   └── klarna_math
│       ├── app.py - main application file
│       ├── __init__.py
├── readme.md - This readme file
├── requirements.txt - requirements pip file
├── tests - Unit tests
│   ├── __init__.py
│   └── tests.py - unitests file
└── wsgi.py - WSGI app file
  

The best routine to install and start server id is as follows:

### Installation:
1. Extract atchive content to a separate newly created directory
2. Open terminal and make sure you're currently located in project directory
3. Create virtual environment by executing command: `virtualenv venv -p [specify python path (e.g. /usr/bin/python3.7)]`
4. Activate your new venv by command `source /venv/bin/activate`
5. Install the required modules by command `pip install -r requirements.txt` the file is attached to the archive

That's it! You're ready to start the application.
To run the application please use gunicorn server:
1. cd to project root directory, where wsgi.py file is located
2. run command gunicorn wsgi:app

you should see in terminal smth like this:
`[2020-02-10 11:32:26 +0300] [25019] [INFO] Starting gunicorn 19.9.0`
`[2020-02-10 11:32:26 +0300] [25019] [INFO] Listening at: http://127.0.0.1:8000 (25019)`
`[2020-02-10 11:32:26 +0300] [25019] [INFO] Using worker: sync`
`[2020-02-10 11:32:26 +0300] [25022] [INFO] Booting worker with pid: 25022`
`Scheduler started`

### Using the application
Application is using Swagger/OAS notation to describe the API, to see the swagger page just visit http://127.0.0.1:8000
This page contains full API description with parameters and URLs

At the moment there're 3 web services presenting: Fibonacci, Factorial and Ackermann functions.
For example to call the Ackermann function for m=3 and n=3 - make get request: [http://127.0.0.1:8000/klarna_math/ackermann/3/3]
The result has json format with schema:

response_schema = {
    "result": {"type": "number"},
    "time_elapsed": {"type": "number"}
}

result - the calculated function value
time_elapsed - time spent on execution

More to that a took decision to attach special monitoring system to the project.
It is Flask-MonitoringDashboard module, starts alongside main app and can be accessed via http://127.0.0.1:8000/dashboard. Default password/login are admin/admin
It drives special web interface with different statistics on funtions runs, time elapsed statistics with aggregations by dates, hours etc.
Really cool thing!

### Testing the app
I created a number of tests for every function. They a stored at /tests directory
Tests are developed using standard unittest python module
To run tests please execute terminal command `python -m unittest tests/tests.py -v` you will get the following output:

Tests execution is also monitored and can be seen in dashboard.
