from unittest import TestCase
from core import create_app
import jsonschema
import json

app = create_app()

# Defining schemas for validating response formats
response_schema = {
    "result": {"type": "number"},
    "time_elapsed": {"type": "number"}
}

error_response_schema = {
    "error": {"type": "string"}
}


class TestFibonacci(TestCase):
    def setUp(self):
        """ Setting Flask testing client"""
        app.config['TESTING'] = True
        app.config['WTF_CSRF_ENABLED'] = False
        app.config['DEBUG'] = False
        self.app = app.test_client()
        self.assertEqual(app.debug, False)

    def test_error_response_format(self):
        response = self.app.get('/klarna_math/fibonacci/-1')
        self.assertEqual(jsonschema.validate(
            response.data, error_response_schema), None)

    def test_wrong_parameter_type(self):
        response = self.app.get('/klarna_math/fibonacci/s')
        self.assertEqual(response.status_code, 500)

    def test_wrong_parameter_negative(self):
        response = self.app.get('/klarna_math/fibonacci/-1')
        self.assertEqual(response.status_code, 500)

    def test_response_status_code(self):
        response = self.app.get('/klarna_math/fibonacci/4')
        self.assertEqual(response.status_code, 200)

    def test_response_format(self):
        response = self.app.get('/klarna_math/fibonacci/5')
        self.assertEqual(jsonschema.validate(
            response.data, response_schema), None)

    def test_result(self):
        response = self.app.get('/klarna_math/fibonacci/6')
        result = json.loads(response.data).get('result')
        self.assertEqual(result, 8)

    def test_result_big(self):
        response = self.app.get('/klarna_math/fibonacci/200')
        result = json.loads(response.data).get('result')
        self.assertEqual(result, 280571172992512015699912586503521287798784)


class TestAckermann(TestCase):
    def setUp(self):
        """ Setting Flask testing client"""
        app.config['TESTING'] = True
        app.config['WTF_CSRF_ENABLED'] = False
        app.config['DEBUG'] = False
        self.app = app.test_client()
        self.assertEqual(app.debug, False)

    def test_error_response_format(self):
        response = self.app.get('/klarna_math/ackermann/-1/2')
        self.assertEqual(jsonschema.validate(
            response.data, error_response_schema), None)

    def test_missing_parameter(self):
        response = self.app.get('/klarna_math/ackermann/2')
        self.assertEqual(jsonschema.validate(
            response.data, error_response_schema), None)

    def test_wrong_parameter_type_1(self):
        response = self.app.get('/klarna_math/ackermann/s/2')
        self.assertEqual(jsonschema.validate(
            response.data, error_response_schema), None)

    def test_wrong_parameter_type_2(self):
        response = self.app.get('/klarna_math/ackermann/2/s')
        self.assertEqual(jsonschema.validate(
            response.data, error_response_schema), None)

    def test_wrong_parameter_negative_1(self):
        response = self.app.get('/klarna_math/ackermann/-1/2')
        self.assertEqual(response.status_code, 500)

    def test_wrong_parameter_negative_2(self):
        response = self.app.get('/klarna_math/ackermann/1/-2')
        self.assertEqual(response.status_code, 500)

    def test_response_status_code(self):
        response = self.app.get('/klarna_math/ackermann/2/4')
        self.assertEqual(response.status_code, 200)

    def test_response_format(self):
        response = self.app.get('/klarna_math/ackermann/2/4')
        self.assertEqual(jsonschema.validate(
            response.data, response_schema), None)

    def test_result(self):
        response = self.app.get('/klarna_math/ackermann/2/2')
        result = json.loads(response.data).get('result')
        self.assertEqual(result, 7)

    def test_result_big(self):
        response = self.app.get('/klarna_math/ackermann/3/7')
        result = json.loads(response.data).get('result')
        self.assertEqual(result, 1021)


class TestFactorial(TestCase):
    def setUp(self):
        """ Setting Flask testing client"""
        app.config['TESTING'] = True
        app.config['WTF_CSRF_ENABLED'] = False
        app.config['DEBUG'] = False
        self.app = app.test_client()
        self.assertEqual(app.debug, False)

    def test_error_response_format(self):
        response = self.app.get('/klarna_math/factorial/-1')
        self.assertEqual(jsonschema.validate(
            response.data, error_response_schema), None)

    def test_wrong_parameter_type(self):
        response = self.app.get('/klarna_math/factorial/s')
        self.assertEqual(response.status_code, 500)

    def test_wrong_parameter_negative(self):
        response = self.app.get('/klarna_math/factorial/-1')
        self.assertEqual(response.status_code, 500)

    def test_response_status_code(self):
        response = self.app.get('/klarna_math/factorial/4')
        self.assertEqual(response.status_code, 200)

    def test_response_format(self):
        response = self.app.get('/klarna_math/factorial/5')
        self.assertEqual(jsonschema.validate(
            response.data, response_schema), None)

    def test_result(self):
        response = self.app.get('/klarna_math/factorial/10')
        result = json.loads(response.data).get('result')
        self.assertEqual(result, 3628800)

    def test_result_big(self):
        response = self.app.get('/klarna_math/factorial/25')
        result = json.loads(response.data).get('result')
        self.assertEqual(result, 15511210043330985984000000)
